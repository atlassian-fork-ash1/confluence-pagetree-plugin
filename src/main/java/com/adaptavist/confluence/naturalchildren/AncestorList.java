package com.adaptavist.confluence.naturalchildren;

import javax.annotation.Nonnull;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * AncestorList supports a properly order of ancestors
 * Different implementations are able to parse ancestors from different sources
 * like from search index or from the hibernate Page object
 * So finally we get ancestors in properly order
 *
 * @since 4.0.0
 */
public abstract class AncestorList implements Iterable<Long> {
    // we would like to prevent duplicates, so we have to use LinkedHashSet
    protected Set<Long> ancestors = new LinkedHashSet<>();

    protected void addAncestor(long id) {
        this.ancestors.add(id);
    }

    @Override
    @Nonnull
    public Iterator<Long> iterator() {
        return ancestors.iterator();
    }
}
