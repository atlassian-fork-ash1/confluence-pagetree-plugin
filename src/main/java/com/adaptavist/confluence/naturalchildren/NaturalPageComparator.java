package com.adaptavist.confluence.naturalchildren;

import com.atlassian.confluence.core.ContentEntityObject;
import com.eekboom.utils.Strings;

import java.util.Comparator;

/**
 * This comparator somewhat overlaps with the {@link com.atlassian.confluence.pages.ChildPositionComparator} inside confluence.
 *
 * @since 1.0
 */
public class NaturalPageComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        String string1, string2;

        string1 = getNaturalTitle(o1);
        string2 = getNaturalTitle(o2);

        return Strings.compareNatural(string1, string2);
    }

    private String getNaturalTitle(Object object) {
        if (object instanceof ContentEntityObject) {
            return ((ContentEntityObject) object).getTitle();
        }
        return "";
    }
}
