package com.adaptavist.confluence.naturalchildren;

import com.atlassian.confluence.pages.Page;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * PageTree allows to build page tree based on the search results
 * It also allows to get either all root pages or all children of any node
 *
 * @since 4.0.0
 */
public class PageTree {
    static class PageTreeNode {
        private PageTreeNode(long id) {
            this.id = id;
        }

        long id;
        Page page;
        Map<Long, PageTreeNode> children;

        PageTreeNode addChildIfDoesNotExist(long childId) {
            if (children == null) {
                children = new LinkedHashMap<>();
            }
            PageTreeNode childNode = children.get(childId);
            if (childNode == null) {
                childNode = new PageTreeNode(childId);
                children.put(childId, childNode);
            }

            return childNode;
        }
    }

    public boolean hasChildren(Long id) {
        return nodesWithChildren.get(id) != null;
    }

    public void addPage(AncestorList ancestorList, Page page) {
        Iterator<Long> iterator = ancestorList.iterator();
        final long rootId = iterator.next();
        PageTreeNode parentNode = topLevelPages.get(rootId);
        if (parentNode == null) {
            parentNode = new PageTreeNode(rootId);
            topLevelPages.put(rootId, parentNode);
        }

        while (iterator.hasNext()) {
            Long currentId = iterator.next();
            if (parentNode.children == null) {
                nodesWithChildren.put(parentNode.id, parentNode);
            }
            parentNode = parentNode.addChildIfDoesNotExist(currentId);
        }

        parentNode.page = page;
    }

    private Map<Long, PageTreeNode> topLevelPages = new LinkedHashMap<>();
    // nodesWithChildren allows to find any page in the tree with O(1) complexity
    // it is useful when we either get children or just check the existence of children
    private Map<Long, PageTreeNode> nodesWithChildren = new HashMap<>();

    public List<Page> getTopLevelPageList() {
        return topLevelPages.values().stream().map(node -> node.page).collect(Collectors.toList());
    }

    public List<Page> getChildrenPages(long pageId) {
        PageTreeNode currentNode = nodesWithChildren.get(pageId);
        if (currentNode == null) {
            return Collections.emptyList();
        }
        return currentNode.children.values().stream()
                .filter(node -> node.page != null)
                .map(node -> node.page)
                .collect(Collectors.toList());
    }
}
