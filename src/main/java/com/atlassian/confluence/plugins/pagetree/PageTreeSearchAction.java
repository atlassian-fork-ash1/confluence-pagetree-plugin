/*
 * Copyright (c), Shannon Krebs
 * Refer to bundled license.txt file for license information
 */

package com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.core.ConfluenceActionSupport;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.atlassian.confluence.util.HtmlUtil.urlEncode;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Simple action to construct a pagetreesearch query string to pass to the
 * dosearchsite action.
 *
 * @author Shannon Krebs
 * @since 1.0
 */
public class PageTreeSearchAction extends ConfluenceActionSupport {

    private String queryString;
    private String ancestorId;

    private String searchActionString;

    private String spaceKey;

    public String execute() throws Exception {
        StringBuilder actionString = new StringBuilder("/dosearchsite.action?searchQuery.queryString=");
        List<String> searchTerms = new ArrayList<>();

        if (ancestorId != null && ancestorId.length() > 0) {
            searchTerms.add("ancestorIds%3A" + ancestorId);
        }

        if (queryString != null && queryString.length() > 0) {
            searchTerms.add(urlEncode(queryString));
        }

        for (Iterator iter = searchTerms.iterator(); iter.hasNext(); ) {
            String searchTerm = (String) iter.next();

            actionString.append(searchTerm);
            if (iter.hasNext()) {
                actionString.append("+AND+");
            }
        }

        // Add the space key
        actionString.append("&searchQuery.spaceKey=");
        if (isNotEmpty(spaceKey)) {
            actionString.append(urlEncode(spaceKey));
        } else {
            actionString.append(urlEncode("conf_all"));
        }

        searchActionString = actionString.toString();

        return "search";
    }

    public String getAncestorId() {
        return ancestorId;
    }

    public void setAncestorId(String ancestorId) {
        this.ancestorId = ancestorId;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getSearchActionString() {
        return searchActionString;
    }

    public void setSearchActionString(String searchActionString) {
        this.searchActionString = searchActionString;
    }

    public String getSpaceKey() {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

}
