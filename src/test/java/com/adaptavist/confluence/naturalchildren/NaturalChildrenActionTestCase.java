package com.adaptavist.confluence.naturalchildren;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.internal.ContentPermissionManagerInternal;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionCheckExemptions;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NaturalChildrenActionTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private XhtmlContent xhtmlContent;
    @Mock
    private PageManager pageManager;
    @Mock
    private SpaceManager spaceManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ContentPermissionManagerInternal contentPermissionManager;
    @Mock
    private PermissionCheckExemptions permissionCheckExemptions;
    @Mock
    private ExcerptHelper excerptHelper;

    private String spaceKey = "tst";
    private Space space;
    private Page page;
    private NaturalChildrenAction naturalChildrenAction;

    @Before
    public void setUp() {
        space = new Space(spaceKey);
        page = new Page();
        naturalChildrenAction = new NaturalChildrenAction();
        setNaturalChildrenActionDependencies(naturalChildrenAction);
    }

    @Test
    public void testPageExcerptHtml() throws XhtmlException, XMLStreamException {
        String excerpt = "true";
        String excerptAfterConvert = "This is after convert excerpt";

        when(excerptHelper.getExcerpt(page)).thenReturn(excerpt);
        when(xhtmlContent.convertStorageToView(any(String.class), any(ConversionContext.class))).thenReturn(excerptAfterConvert);
        when(pageManager.getPage(eq(page.getId()))).thenReturn(page);
        assertEquals(excerptAfterConvert, naturalChildrenAction.getPageExcerptHtml(page));
    }

    @Test
    public void testPageExcerptHtmlWithException() throws XhtmlException, XMLStreamException {
        String excerpt = "<b>test</b>";
        String excerptWithException = "<span class=\"error\">&lt;b&gt;test&lt;/b&gt;</span>";

        when(excerptHelper.getExcerpt(page)).thenReturn(excerpt);
        when(xhtmlContent.convertStorageToView(any(String.class), any(ConversionContext.class))).thenThrow(new XMLStreamException("xmlStreamException"));
        when(pageManager.getPage(eq(page.getId()))).thenReturn(page);

        assertEquals(excerptWithException, naturalChildrenAction.getPageExcerptHtml(page));
    }

    @Test
    public void testPageExcerptHtmlWithNullExcerpt() {
        assertEquals("", naturalChildrenAction.getPageExcerptHtml(page));
    }

    @Test
    public void testGetAllStartPagesWithoutSort() {
        List<Page> expectedStartPages = new ArrayList<Page>();

        Space testSpace = new Space();

        Page homePage = new Page();
        homePage.setTitle("Home Page");

        testSpace.setHomePage(homePage);

        Page pageOne = new Page();
        pageOne.setTitle("One");

        Page pageTwo = new Page();
        pageTwo.setTitle("Two");

        expectedStartPages.add(pageOne);
        expectedStartPages.add(pageTwo);
        expectedStartPages.add(homePage);

        when(spaceManager.getSpace(spaceKey)).thenReturn(testSpace);
        when(pageManager.getTopLevelPages(testSpace)).thenReturn(expectedStartPages);
        when(permissionManager.getPermittedEntities(eq(AuthenticatedUserThreadLocal.get()), eq(Permission.VIEW), eq(expectedStartPages)))
                .thenReturn(expectedStartPages);
        when(contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(
                any(), any(), any())).thenReturn(expectedStartPages);

        naturalChildrenAction.setSort("");
        naturalChildrenAction.setReverse(false);
        assertEquals(expectedStartPages, naturalChildrenAction.getAllPermittedStartPages(spaceKey));
    }

    /**
     * Checks that the list of pages is limited and "Show all" button is rendered
     * when it is rendered on the sidebar
     */
    @Test
    public void testGetPermittedChildrenWithShowMoreButtonOnSidebar() {
        naturalChildrenAction.setPlacement("sidebar");
        when(contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(any(), any(), any())).
                thenAnswer((Answer<List<Page>>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (List<Page>) args[0];
                });

        Page parentPage = new Page();
        parentPage.setTitle("Page Level 1");
        for (int i = 0; i < NaturalChildrenAction.BATCH_SIZE + 5; i++) {
            Page childPage = new Page();
            childPage.setTitle("Child page " + i);
            childPage.setParentPage(parentPage);
            parentPage.addChild(childPage);
        }

        PageList pageList = naturalChildrenAction.getLimitedPermittedChildren(parentPage);
        assertEquals(NaturalChildrenAction.BATCH_SIZE + 1, pageList.getPageList().size());
        assertFalse(pageList.isHasMoreBefore());
        assertTrue(pageList.isHasMoreAfter());
    }

    /**
     * Checks that the whole list of pages is displayed and "Show all" buttons are not rendered
     * when it is rendered in the page tree macro (placement is not "sidebar)
     */
    @Test
    public void testGetPermittedChildrenWithoutShowMoreButtonInPageTreeMacro() {
        naturalChildrenAction.setPlacement(null);
        when(contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(any(), any(), any())).
                thenAnswer((Answer<List<Page>>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (List<Page>) args[0];
                });

        Page parentPage = new Page();
        parentPage.setTitle("Page Level 1");
        final int NUMBER_OF_PAGES = NaturalChildrenAction.BATCH_SIZE + 5;
        for (int i = 0; i < NUMBER_OF_PAGES; i++) {
            Page childPage = new Page();
            childPage.setTitle("Child page " + i);
            childPage.setParentPage(parentPage);
            parentPage.addChild(childPage);
        }

        PageList pageList = naturalChildrenAction.getLimitedPermittedChildren(parentPage);
        assertEquals(NUMBER_OF_PAGES, pageList.getPageList().size());
        assertFalse(pageList.isHasMoreBefore());
        assertFalse(pageList.isHasMoreAfter());
    }

    @Test
    public void testGetPermittedChildrenPageByReverseSort() {
        final List<Page> permittedChildren = new ArrayList<Page>();

        List<Object> expectedPermittedPage = new ArrayList<Object>();

        Page parentPage = new Page();
        parentPage.setTitle("Page Level 1");

        Page childPage = new Page();
        childPage.setTitle("Childpage");
        childPage.setParentPage(parentPage);

        Page childPageTwo = new Page();
        childPageTwo.setTitle("Childpage Two");
        childPageTwo.setParentPage(parentPage);

        permittedChildren.add(childPage);
        permittedChildren.add(childPageTwo);

        expectedPermittedPage.add(childPageTwo);
        expectedPermittedPage.add(childPage);

        naturalChildrenAction.setSort("");
        naturalChildrenAction.setReverse(true);
        naturalChildrenAction.setPage(parentPage);

        when(permissionManager.hasPermission(eq(AuthenticatedUserThreadLocal.get()),
                eq(Permission.VIEW), eq(parentPage))).thenReturn(true);
        when(permissionCheckExemptions.isExempt(eq(AuthenticatedUserThreadLocal.get()))).thenReturn(false);
        when(contentPermissionManager.getPermittedChildren(eq(parentPage), eq(AuthenticatedUserThreadLocal.get()))).thenReturn(permittedChildren);
        when(contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(
                any(), any(), any())).thenReturn(permittedChildren);

        assertEquals(expectedPermittedPage, naturalChildrenAction.getPermittedChildren(parentPage));
    }

    @Test
    public void testCurrentPageIsAncestorPage() {
        String[] ancestors = {"100"};

        Page testPage = new Page() {
            @Override
            public String getIdAsString() {
                return "100";
            }
        };

        naturalChildrenAction.setAncestors(ancestors);
        assertTrue(naturalChildrenAction.isAncestorPage(testPage));
    }

    @Test
    public void testCurrentPageIsNotAncestorPage() {
        String[] ancestors = {"200"};

        Page testPage = new Page() {
            @Override
            public String getIdAsString() {
                return "100";
            }
        };

        naturalChildrenAction.setAncestors(ancestors);
        assertFalse(naturalChildrenAction.isAncestorPage(testPage));
    }

    @Test
    public void testCurrentPageNotSetToExpand() throws Exception {
        long id = 100;
        when(permissionManager.hasPermission(eq(AuthenticatedUserThreadLocal.get()),
                eq(Permission.VIEW), eq(space))).thenReturn(true);
        naturalChildrenAction.setSpace(space);
        naturalChildrenAction.setExpandCurrent(false);
        naturalChildrenAction.setTreePageId(id);
        naturalChildrenAction.doDefault();
        assertFalse(naturalChildrenAction.getIdsToExpand().contains(id));
    }

    @Test
    public void testCurrentPageSetToExpand() throws Exception {
        long id = 100;
        when(permissionManager.hasPermission(eq(AuthenticatedUserThreadLocal.get()),
                eq(Permission.VIEW), eq(space))).thenReturn(true);
        naturalChildrenAction.setSpace(space);
        naturalChildrenAction.setExpandCurrent(true);
        naturalChildrenAction.setTreePageId(id);
        naturalChildrenAction.doDefault();
        assertTrue(naturalChildrenAction.getIdsToExpand().contains(id));
    }

    @Test
    public void testGetAllStartPagesReturnsTopLevelPagesAndNotOrphanedPages() {
        Space aSpace = new Space(spaceKey);
        Page theOnlyTopLevelPage = new Page();

        theOnlyTopLevelPage.setSpace(aSpace);
        theOnlyTopLevelPage.setTitle("Top level page 1");

        when(spaceManager.getSpace(spaceKey)).thenReturn(aSpace);
        when(pageManager.getTopLevelPages(aSpace)).thenReturn(singletonList(theOnlyTopLevelPage));
        when(permissionManager.getPermittedEntities(eq(AuthenticatedUserThreadLocal.get()), eq(Permission.VIEW), eq(singletonList(theOnlyTopLevelPage))))
                .thenReturn(singletonList(theOnlyTopLevelPage));
        when(contentPermissionManager.getPermittedPagesIgnoreInheritedPermissions(
                any(), any(), any())).thenReturn(singletonList(theOnlyTopLevelPage));

        List allStartPages = naturalChildrenAction.getAllPermittedStartPages(spaceKey);

        assertEquals(singletonList(theOnlyTopLevelPage), allStartPages);
        verify(pageManager, never()).getOrphanedPages(anyString());
    }

    @Test
    public void testExcerptHtmlEmptyIfContentExcerptIsEmptyString() {
        when(excerptHelper.getExcerpt(page)).thenReturn("");
        when(pageManager.getPage(eq(page.getId()))).thenReturn(page);
        assertTrue(StringUtils.isBlank(naturalChildrenAction.getPageExcerptHtml(page)));
    }

    private void setNaturalChildrenActionDependencies(NaturalChildrenAction naturalChildrenAction) {
        naturalChildrenAction.setExcerptHelper(excerptHelper);
        naturalChildrenAction.setContentPermissionManager(contentPermissionManager);
        naturalChildrenAction.setPermissionCheckExemptions(permissionCheckExemptions);
        naturalChildrenAction.setXhtmlContent(xhtmlContent);
        naturalChildrenAction.setPageManager(pageManager);
        naturalChildrenAction.setSpaceManager(spaceManager);
        naturalChildrenAction.setPermissionManager(permissionManager);
        naturalChildrenAction.setSpaceKey(spaceKey);
        naturalChildrenAction.setExcerpt(false);
        naturalChildrenAction.setSort("");
        naturalChildrenAction.setReverse(false);
        naturalChildrenAction.setTreeId("10");
        naturalChildrenAction.setStartDepth(0);
        naturalChildrenAction.setHasRoot(false);
        naturalChildrenAction.setDisableLinks(false);
        naturalChildrenAction.setExpandCurrent(false);
        naturalChildrenAction.setTreePageId(null);
    }
}
