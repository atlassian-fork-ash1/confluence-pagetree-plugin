package com.atlassian.confluence.plugins.pagetree;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputDeviceType;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderMode;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PageTreeSearchMacroTestCase {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PageManager pageManager;
    @Mock
    private SpaceManager spaceManager;
    @Mock
    private SettingsManager settingsManager;
    @Mock
    private Settings settings;
    @Mock
    private PageContext pageContext;
    @Mock
    private ConversionContext conversionContext;
    @Mock
    private WebResourceManager webResourceManager;

    private Map<String, String> params;
    private Map<String, Object> velocityContext;
    private String baseUrl = "www.atlassian.com";

    @Before
    public void setUp() {
        params = new HashMap<>();
        velocityContext = new HashMap<>();

        when(settingsManager.getGlobalSettings()).thenReturn(settings);
    }

    @Test
    public void testCurrentPageUsedAsRootIfRootPageStringIsNull() {
        final String spaceKey = "tst";
        final String outputType = "random output type";
        final Space testSpace = new Space(spaceKey);
        final Page testPage = new Page();
        testPage.setSpace(testSpace);
        testPage.setTitle("TestPage");

        PageTreeSearchMacro pageTreeSearchMacro = new TestPageTreeSearchMacro() {
            @Override
            protected String getRenderedTemplateWithoutSwallowingErrors(
                    Map velocityContext) {
                assertEquals(testPage, velocityContext.get("rootPage"));
                assertEquals(spaceKey, velocityContext.get("spaceKey"));
                assertEquals(0, ((List) velocityContext.get("errors")).size());
                assertEquals(new ArrayList<String>(), velocityContext.get("errors"));
                assertEquals(outputType, velocityContext.get("outputType"));
                assertEquals(baseUrl, velocityContext.get("baseUrl"));

                return "";
            }
        };

        when(conversionContext.getEntity()).thenReturn(testPage);
        when(conversionContext.getOutputType()).thenReturn(outputType);
        when(settings.getBaseUrl()).thenReturn(baseUrl);

        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);

        assertEquals("", pageTreeSearchMacro.execute(params, "", conversionContext));

        verify(spaceManager, never()).getSpace(anyString());
        verify(pageManager, never()).getPage(anyString(), anyString());
    }

    @Test
    public void testErrorMessageShownIfMacroRenderedOnNonPageContentAndRootStringNotSpecified() {
        final String outputType = "random output type";
        final BlogPost testBlogPost = new BlogPost();
        testBlogPost.setTitle("TestBlogPost");

        final List<String> errorList = new ArrayList<String>();
        errorList.add("pagetreesearch.rootpage.invalidpage");

        PageTreeSearchMacro pageTreeSearchMacro = new TestPageTreeSearchMacro() {
            @Override
            protected String getRenderedTemplateWithoutSwallowingErrors(
                    Map velocityContext) {
                assertNull(velocityContext.get("rootPage"));
                assertNull(velocityContext.get("spaceKey"));
                assertEquals(1, ((List) velocityContext.get("errors")).size());
                assertEquals(errorList, velocityContext.get("errors"));
                assertEquals(outputType, velocityContext.get("outputType"));
                assertEquals(baseUrl, velocityContext.get("baseUrl"));

                return "";
            }
        };

        when(conversionContext.getEntity()).thenReturn(testBlogPost);
        verify(pageContext, never()).getSpaceKey();
        when(conversionContext.getOutputType()).thenReturn(outputType);
        when(settings.getBaseUrl()).thenReturn(baseUrl);

        assertEquals("", pageTreeSearchMacro.execute(params, "", conversionContext));

        verify(spaceManager, never()).getSpace(anyString());
        verify(pageManager, never()).getPage(anyString(), anyString());
    }

    @Test
    public void testFindPageInCurrentSpaceWhenRootPageStringIsNotNull() {
        final String spaceKey = "tst";
        final String outputType = "random output type";
        final Space space = new Space();
        final Page page = new Page();
        space.setKey(spaceKey);
        space.setName(spaceKey);
        page.setTitle("Test");

        params.put("rootPage", ":Test");

        PageTreeSearchMacro pageTreeSearchMacro = new TestPageTreeSearchMacro() {
            @Override
            protected String getRenderedTemplateWithoutSwallowingErrors(
                    Map velocityContext) {
                assertEquals(page, velocityContext.get("rootPage"));
                assertEquals(spaceKey, velocityContext.get("spaceKey"));
                assertEquals(0, ((List) velocityContext.get("errors")).size());
                assertEquals(new ArrayList<String>(), velocityContext.get("errors"));
                assertEquals(outputType, velocityContext.get("outputType"));
                assertEquals(baseUrl, velocityContext.get("baseUrl"));

                return "";
            }
        };

        when(conversionContext.getOutputType()).thenReturn(outputType);
        when(conversionContext.getSpaceKey()).thenReturn(spaceKey);
        when(conversionContext.getEntity()).thenReturn(page);
        when(pageManager.getPage(spaceKey, "Test")).thenReturn(page);
        when(settings.getBaseUrl()).thenReturn(baseUrl);

        assertEquals("", pageTreeSearchMacro.execute(params, "", conversionContext));

        verify(pageContext, never()).getEntity();
    }

    @Test
    public void testShowErrorMessageWhenSpaceAndPageIsNotFound() {
        final String outputType = "random output type";

        params.put("rootPage", "tst:Test");

        final List<String> errorList = new ArrayList<>();
        errorList.add("pagetreesearch.rootspace.notfound");
        errorList.add("pagetreesearch.rootpage.notfound");

        PageTreeSearchMacro pageTreeSearchMacro = new TestPageTreeSearchMacro() {
            @Override
            protected String getRenderedTemplateWithoutSwallowingErrors(
                    Map velocityContext) {
                assertNull(velocityContext.get("rootPage"));
                assertEquals("tst", velocityContext.get("spaceKey"));
                assertEquals(2, ((List) velocityContext.get("errors")).size());
                assertEquals(errorList, velocityContext.get("errors"));
                assertEquals(outputType, velocityContext.get("outputType"));
                assertEquals(baseUrl, velocityContext.get("baseUrl"));

                return "";
            }
        };

        when(pageContext.getOutputType()).thenReturn(outputType);
        when(pageContext.getOutputDeviceType()).thenReturn(ConversionContextOutputDeviceType.DESKTOP);
        when(settings.getBaseUrl()).thenReturn(baseUrl);

        assertEquals("", pageTreeSearchMacro.execute(params, "", pageContext));

        verify(pageContext, never()).getEntity();
        verify(pageContext, never()).getSpaceKey();
    }

    @Test
    public void testErrorMessageWhenAnExceptionIsThrown() {
        PageTreeSearchMacro pageTreeSearchMacro = new TestPageTreeSearchMacro() {
            @Override
            protected String getRenderedTemplateWithoutSwallowingErrors(
                    Map velocityContext) throws Exception {
                throw new Exception("Throwing Exception Error");
            }
        };

        when(settings.getBaseUrl()).thenReturn(baseUrl);
        when(pageContext.getOutputDeviceType()).thenReturn(ConversionContextOutputDeviceType.DESKTOP);

        // An empty string will be return after an error occurred
        assertEquals("", pageTreeSearchMacro.execute(params, "", pageContext));
    }

    @Test
    public void testGetBodyRenderMode() {
        PageTreeSearchMacro pageTreeSearchMacro = new TestPageTreeSearchMacro();

        assertEquals(RenderMode.NO_RENDER, pageTreeSearchMacro.getBodyRenderMode());
    }

    @Test
    public void testHasBody() {
        PageTreeSearchMacro pageTreeSearchMacro = new TestPageTreeSearchMacro();

        assertFalse(pageTreeSearchMacro.hasBody());
    }

    @Test
    public void testIsInline() {
        PageTreeSearchMacro pageTreeSearchMacro = new TestPageTreeSearchMacro();

        assertFalse(pageTreeSearchMacro.isInline());
    }

    private class TestPageTreeSearchMacro extends PageTreeSearchMacro {
        private TestPageTreeSearchMacro() {
            setPageManager(pageManager);
            setSpaceManager(spaceManager);
            setSettingsManager(settingsManager);
            setWebResourceManager(webResourceManager);
        }

        @Override
        protected Map<String, Object> getDefaultVelocityContext() {
            return velocityContext;
        }
    }
}
