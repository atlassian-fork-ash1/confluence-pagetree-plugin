package it.com.atlassian.confluence.plugins.pagetree;

import java.io.File;
import java.io.IOException;

public class PageTreeSearchMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private String spaceKey = "ds";

    public void testRenderedPageTreeSearchMacro() {
        final long pageId = createPage(spaceKey, "testRenderedPageTreeSearchMacro", "{pagetreesearch}");

        viewPageById(pageId);

        assertElementPresentByXPath("//div[@class='wiki-content']//div[@id='pagetreesearch']//input[@name='queryString']");
        assertElementPresentByXPath("//div[@class='wiki-content']//div[@id='pagetreesearch']//input[@value='Search']");
    }

    public void testSearchResultWithPageTreeSearch() {
        setConfluenceBaseUrl(getSpaceHelper(spaceKey).getConfluenceWebTester().getBaseUrl());

        final long pageId = createPage(spaceKey, "testSearchResultWithPageTreeSearch", "{pagetreesearch}");

        getIndexHelper().update();

        viewPageById(pageId);

        String ancestorId = getElementAttributeByXPath("//div[@class='wiki-content']//div[@id='pagetreesearch']//form[@name='pagetreesearchform']//input[@name='ancestorId']", "value");

        setWorkingForm("pagetreesearchform");
        setTextField("queryString", "testSearchResultWithPageTreeSearch");
        clickElementByXPath("//div[@class='wiki-content']//div[@id='pagetreesearch']//form//input[@type='submit']");

        assertEquals("ancestorIds:" + ancestorId + " AND testSearchResultWithPageTreeSearch",
                getElementAttributeByXPath("//input[@id='query-string']", "value"));

        assertEquals("testSearchResultWithPageTreeSearch",
                getElementTextByXPath("//ol[starts-with(@class, 'search-results') and not(starts-with(@class, 'search-results-'))]/li[1]//h3/a"));

        clickLinkWithText("testSearchResultWithPageTreeSearch");
                assertEquals(spaceKey,
                    getElementAttributeByXPath("//meta[@id='confluence-space-key']", "content")
        );
    }

    public void testSpaceKeyHtmlEncoded() throws IOException
    {
        File testSiteExport = copyClasspathResourceToTempFile("PGTR-68-export.zip", "it.com.atlassian.confluence.plugins.pagetree", ".zip");

        getConfluenceWebTester().restoreData(testSiteExport);

        gotoPage("/pages/viewpage.action?pageId=655366");
        assertEquals("~\"><<script>alert('hahahaha')</script>", getElementAttributeByXPath("//div[@class='wiki-content']//form[@name='pagetreesearchform']//input[@name='spaceKey']", "value"));
    }


}
