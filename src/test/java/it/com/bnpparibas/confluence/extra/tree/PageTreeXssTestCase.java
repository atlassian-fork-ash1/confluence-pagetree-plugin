package it.com.bnpparibas.confluence.extra.tree;

import it.com.atlassian.confluence.plugins.pagetree.AbstractConfluencePluginWebTestCaseBase;

import java.io.IOException;
import java.io.File;

public class PageTreeXssTestCase extends AbstractConfluencePluginWebTestCaseBase
{
    private static final String SPACE_KEY = "ds";
    private static final String EVIL_PARAMETER = "x\"</pre><script>alert(1)</script>";

    public void testSpaceKeyHtmlEncoded() throws IOException
    {
        File testSiteExport = copyClasspathResourceToTempFile("PGTR-68-export.zip", "it.com.atlassian.confluence.plugins.pagetree", ".zip");

        getConfluenceWebTester().restoreData(testSiteExport);

        gotoPage("/pages/viewpage.action?pageId=917507");

        assertEquals("~\"><<script>alert('hahahaha')</script>", getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//form[@name='pagetreesearchform']//input[@name='spaceKey']", "value"));
        assertEquals("~\"><<script>alert('hahahaha')</script>", getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='spaceKey']", "value"));
    }

    public void testSortParameterHtmlEncoded() {
        final long pageId = createPage(SPACE_KEY, "sortParameterXssTest", "{pagetree:sort=" + EVIL_PARAMETER + "}");
        testEvilParameterEscaped(pageId);
    }

    public void testReverseParameterHtmlEncoded() {
        final long pageId = createPage(SPACE_KEY, "reverseParameterXssTest", "{pagetree:reverse=" + EVIL_PARAMETER + "}");
        testEvilParameterEscaped(pageId);
    }

    public void testExcerptParameterHtmlEncoded() {
        final long pageId = createPage(SPACE_KEY, "excerptParameterXssTest", "{pagetree:excerpt=" + EVIL_PARAMETER + "}");
        testEvilParameterEscaped(pageId);
    }

    private void testEvilParameterEscaped(long pageId)
    {
        getIndexHelper().update();
        viewPageById(pageId);
        assertTrue(getElementAttributeByXPath("//div[@class='wiki-content']//div[contains(@class, 'plugin_pagetree') and not(contains(@class, 'plugin_pagetree_'))]//fieldset//input[@name='treeRequestId']", "value").contains(EVIL_PARAMETER));
    }
}
